import axios from 'axios'

const API_KEY = process.env.REACT_APP_API_KEY
const API_URL = process.env.REACT_APP_API_URL
const ROOT_URL = process.env.REACT_APP_ROOT_URL

export function connectToLastfm() {
  const urlWithCallback = `https://www.last.fm/api/auth/?api_key=${API_KEY}&cb=${ROOT_URL}`
  window.location.replace(urlWithCallback)
}

export function getTrackData(user) {
  console.log('api', API_URL)
  return axios.get(`${API_URL}/current/${user}`, { withCredentials: true })
}

export function scrobbleTrack(track) {
  return axios.post(`${API_URL}/scrobble/`, { track }, { withCredentials: true })
}

export function handleToken(token) {
    return new Promise((resolve) =>
      axios.get(`${API_URL}/token/${token}`, { withCredentials: true })
        .then(response => resolve(response.data))
        .catch(response => console.log(response))
    )
}
