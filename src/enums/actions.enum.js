
export const ActionTypes = {
    SetParty: 'SET_PARTY',
    GetTrack: 'GET_TRACK',
    NewTrack: 'NEW_TRACK',
    Authenticated: 'AUTHENTICATED',
    SetErrorMessage: 'SET_ERRORMSG'
}
Object.freeze(ActionTypes)
