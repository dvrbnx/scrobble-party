
export const ErrorTypes = {
  General: 'GENERAL',
  UserNotFound: 'USERNOTFOUND',
  NoTracks: 'NOTRACKS'
}
Object.freeze(ErrorTypes)
