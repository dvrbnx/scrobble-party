import React from 'react'
import './index.scss'
import queryString from 'query-string'

// Services
import { handleToken } from './services/lastfm'

// Store
import { setAuthenticated, setErrorMessage } from './store/lastfm.actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Main from './components/main'

const REACT_APP_ROOT_URL = process.env.REACT_APP_ROOT_URL

export class App extends React.Component<Props> {

    componentDidMount() {
      this.props.setErrorMessage(null)

      const qs = queryString.parse(window.location.search)
      if (qs.token) {
        handleToken(qs.token).then(response => {
          const success = response.user !== undefined
          this.props.setAuthenticated({
            authenticated: success,
            user: response.user
          })
          window.location.replace(REACT_APP_ROOT_URL)
        })
      }
    }

    render() {
      return(<Main />)
    }
}

const mapStateToProps = (state, ownProps) => ({
  currentTrack: state.currentTrack
})

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ setAuthenticated, setErrorMessage }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
