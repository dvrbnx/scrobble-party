import React from 'react'
import BuyMeACoffee from './buy-me-a-coffee.jsx';

export default class About extends React.Component<Props> {

    componentDidMount() {

    }

    render() {
        return (
            <div className="d-flex flex-row flex-column-mobile mobile-px-5">
                <section className="w-50 mobile-w-100">
                    <h3>Thanks to</h3>
                    <p>
                        <ul>
                            <li><a href="https://www.last.fm/" target="_blank">Last.fm</a> API</li>
                            <li><a href="https://feathericons.com/" target="_blank">FeatherIcons</a></li>
                            <li><a href="https://openmoji.org/library/emoji-1F389/" target="_blank">OpenMoji</a></li>
                        </ul>
                    </p>
                </section>
                <section className="w-50 mobile-w-100">
                    <h3>About</h3>
                    <p>
                        Made by <a href="https://www.last.fm/user/DRLN" target="_blank">DRLN</a>.<br></br>
                        You can check out the repository (and help out?) <a href="https://gitlab.com/dvrbnx/scrobble-party" target="_blank">here</a>!
                    </p>
                    <p>The only data I need about you is your username, this is stored in the local storage.</p>
                    <p>
                        Creating a web app takes time, effort and money. Feel free to support this project by buying me a coffee. :-)
                    </p>
                    <BuyMeACoffee />
                </section>
            </div>

        )
    }
}
