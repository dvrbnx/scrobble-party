import React from 'react'

export default class BuyMeACoffeeButton extends React.Component<Props> {
    
  componentDidMount() {
      
  }

  render() {
    return (
        <a href="https://www.buymeacoffee.com/drln" target="_blank">
            <img src="https://img.buymeacoffee.com/button-api/?text=Buy me a coffee&emoji=&slug=drln&button_colour=ffffff&font_colour=000000&font_family=Poppins&outline_colour=000000&coffee_colour=FFDD00" />
        </a>
    )
  }
}
