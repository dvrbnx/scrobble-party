import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getTrack } from '../../store/lastfm.actions'
import { subscribe } from 'redux-subscriber'
import moment from 'moment'

export class SongDisplayer extends React.Component<Props> {

  componentDidMount() {
    subscribe('party', state => {
      this.fetchSong(state.party)
    })
    this.interval = setInterval(() => this.fetchSong(this.props.party), 20000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  fetchSong(party) {
    if (!party) {
      return
    }
    this.props.getTrack(party, this.props.apiKey)
  }

  loading() {
    return (!this.props.party || !this.props.currentTrack || !this.props.currentTrack.artistName || !this.props.currentTrack.trackName)
  }

  render() {
    if (!this.props.party) {
        return null
    }
    if (this.loading()) {
        return (
          <div className="h-100 d-flex flex-column justify-content-around align-items-center">
            loading...
          </div>
        )
    }

    return(
        <div className="d-flex flex-column justify-content-around align-items-center">
          <img src={this.props.currentTrack.imgUrl} alt="Album cover"></img>
          <div className="text-center">
            <h1>{this.props.currentTrack.trackName}</h1>
            <h2>{this.props.currentTrack.artistName}</h2>
            <p>
              {this.props.currentTrack.playing
                ? <span>Now playing!</span>
                : <span>{moment.unix(this.props.currentTrack.timestamp).fromNow()}</span>}
            </p>
          </div>
        </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  party: state.party,
  user: state.user,
  currentTrack: state.currentTrack,
  playing: state.playing,
  apiKey: state.apiKey
})

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ getTrack }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SongDisplayer);
