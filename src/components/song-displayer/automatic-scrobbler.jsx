import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { newTrack } from '../../store/lastfm.actions'
import { subscribe } from 'redux-subscriber'
import { scrobbleTrack } from '../../services/lastfm'

export class AutomaticScrobbler extends React.Component<Props> {
  // Scrobble the last track when a new song started
  componentDidMount() {
      subscribe('previousTrack', state => {
        if (this.props.user && state.previousTrack != null) {
          scrobbleTrack(state.previousTrack).then(() => this.props.newTrack())
        }
      })
  }

  render() {
    return ''
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user,
  previousTrack: state.previousTrack
})

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ newTrack }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AutomaticScrobbler)
