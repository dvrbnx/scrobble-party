import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { setParty, setErrorMessage } from '../../store/lastfm.actions'
import { ErrorTypes } from '../../enums/errors.enum'
import { ReactComponent as ArrowRight } from './../../assets/icons/arrow-right.svg';

export class ScrobblerForm extends React.Component<Props> {

  constructor(props) {
    super(props)

    this.partyFieldChange = this.partyFieldChange.bind(this)
    this.submitParty = this.submitParty.bind(this)
  }

  errorMessage() {
    switch(this.props.error) {
      case (ErrorTypes.UserNotFound):
        return <span className="text-danger pb-3">This user was not found (possibly due to privacy settings).</span>
      case (ErrorTypes.NoTrack):
        return <span className="text-danger pb-3">This user hasn't scrobbled any tracks yet.</span>
      case (ErrorTypes.General):
        return <span className="text-danger pb-3">An unforeseen error occured.</span>
      default:
        return null
    }
  }

  partyFieldChange(event) {
    this.setState({ partyField: event.target.value })
    this.props.setErrorMessage(null)
  }

  submitParty(event) {
    this.props.setParty(this.state.partyField)
    event.preventDefault()
  }

  render() {
    if (this.props.party) {
        return null
    }
    return(
      <div className="d-flex flex-column">
        <div className="mt-5">
          <p className="lead text-center mb-0">Who's hosting the party?</p>
          <form className="d-flex flex-wrap flex-column align-items-center justify-content-between" onSubmit={this.submitParty}>
            <div className="input-group mb-3 w-50">
              <input type="text" onChange={this.partyFieldChange} className="form-control" placeholder="username" aria-label="Last.fm username" aria-describedby="button-addon2" />
              <button className="btn btn-outline-light btn-custom" type="button" id="button-addon2">
                <ArrowRight />

              </button>
            </div>
            { this.errorMessage() }
          </form>
          <div className="flex-item"></div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  party: state.party,
  apiKey: state.apiKey,
  error: state.error
})

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ setParty, setErrorMessage }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ScrobblerForm);
