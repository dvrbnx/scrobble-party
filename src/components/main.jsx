import React from 'react'
import { connect } from 'react-redux'

// Components
import SongDisplayer from './song-displayer/song-displayer'
import ScrobblerForm from './scrobbler-form/scrobbler-form'
import AutomaticScrobbler from './song-displayer/automatic-scrobbler'
import About from './about/about'
import Logo from './logo/logo'

// Icons
import { ReactComponent as QuestionMark } from '../assets/icons/help-circle.svg'
import { ReactComponent as ChevronDown } from '../assets/icons/chevron-down.svg'
import HowToUse from './how-to-use/how-to-use'

export class Main extends React.Component<Props> {

    componentDidMount() {
    }

    render() {
        const background = {
            backgroundImage: this.props.currentTrack ? `url(${this.props.currentTrack.imgUrl})` : ''
        }
        return (
            <div>
                <div className="bg-image" style={background}></div>

                <div className="vh-100 w-100 p-5 d-flex flex-column align-items-center justify-content-around">
                    <div>
                        <Logo />
                        <p className="text-center mb-0">
                            Listen to music with your friends and scrobble what they are scrobbling. 
                        </p>
                        <ScrobblerForm />
                        <SongDisplayer />
                        <AutomaticScrobbler />
                    </div>
                    <a href="#HowToUse" className="hover-pointer default-color">
                        <ChevronDown />
                    </a>
                </div>

                <div id="HowToUse" className="vh-100 w-100 color-2 d-flex flex-column align-items-center justify-content-around">
                    <HowToUse />
                    <a href="#About" className="hover-pointer invert-color">
                        <ChevronDown />
                    </a>
                </div>

                <div id="About" className="vh-100 w-100 d-flex flex-column align-items-center justify-content-around">
                    <About />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    currentTrack: state.currentTrack
})

export default connect(mapStateToProps, {})(Main);
