import React from 'react'

export default class HowToUse extends React.Component<Props> {
    
  componentDidMount() {
      
  }

  render() {
    return (
        <div className="p-5">
            <h2 className="pb-3">Why/How to use</h2>
            <p>
                <strong>
                    This does not (yet) work on mobile devices!
                </strong>
            </p>
            <p>
                When you and your friends have Last.fm accounts and you listen
                to music together, only one person gets to scrobble these tracks
                to their account.
            </p>
            <p>
                The Party scrobble allows you to scrobble whatever songs your 
                friend is playing. This way everyone's Last.fm profile is up to date.
            </p>
            <p>
                At the moment this only works on web. 
                Your "host" should have their recent listening setting set to Public.
            </p>
            <p>
                First, connect this page to your last.fm account. Then, enter the username
                of the person who's scrobbling the tracks. 
            </p>
        </div>
    )
  }
}
