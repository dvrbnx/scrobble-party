import React from 'react'

export default class Logo extends React.Component<Props> {
    
  componentDidMount() {   
  }

  render() {
    return (
        <h1 className="text-center display-2">
          <p className="neon title-1">Party</p>
          <p className="neon-broken title-2">Scrobbler</p>
        </h1>
    )
  }
}
