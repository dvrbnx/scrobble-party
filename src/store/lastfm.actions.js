import { ActionTypes } from '../enums/actions.enum'
import { ErrorTypes } from '../enums/errors.enum'
import { getTrackData } from '../services/lastfm'

export const setAuthenticated = (authInfo) => ({
  type: ActionTypes.Authenticated, payload: authInfo
})

export const setParty = (party) => ({
  type: ActionTypes.SetParty, payload: party
})

export const newTrack = (track) => ({
  type: ActionTypes.NewTrack, payload: track
})

export const setErrorMessage = (message) => ({
  type: ActionTypes.SetErrorMessage, payload: message
})

export const getTrack = (party, apikey) => dispatch =>
  getTrackData(party)
    .then(response => {
      if (!response.data.exists) {
        dispatch({ type: ActionTypes.SetParty, payload: null })
        dispatch({ type: ActionTypes.SetErrorMessage, payload: ErrorTypes.NoTrack })
        dispatch({ type: ActionTypes.GetTrack, payload: null })
      } else {
        dispatch({ type: ActionTypes.GetTrack, payload: response.data })
      }
    })
    .catch(error => {
      dispatch({ type: ActionTypes.SetParty, payload: null })
      if (!error.response) {
        dispatch({ type: ActionTypes.SetErrorMessage, payload: ErrorTypes.General })
      }
      else if (error.response.data.error) {
        dispatch({ type: ActionTypes.SetErrorMessage, payload: error.response.data.error })
      } else {
        dispatch({ type: ActionTypes.SetErrorMessage, payload: ErrorTypes.General })
      }
  })
