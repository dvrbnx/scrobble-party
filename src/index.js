import React from 'react'
import ReactDOM from 'react-dom'
import './index.scss'
import { Provider } from 'react-redux'
import App from './app'
import reducer from './store/lastfm.reducer'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { save, load } from 'redux-localstorage-simple'
import initSubscriber from 'redux-subscriber';

const middleware = [ thunk ]
middleware.push(save({
  states: [
    'user',
    'authenticated'
  ]
}))

const createStoreWithMiddleware
    = applyMiddleware(
        ...middleware
    )(createStore)

const store = createStoreWithMiddleware(
  reducer,
  load({
    states: [
      'user',
      'authenticated'
    ]
  }),
  window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
)

initSubscriber(store);

ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root')
);
